var Twit = require('twit')
var express = require('express')
require('dotenv').config()

var secret = {
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token: process.env.ACCESS_TOKEN,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
}

var user_id = '3190359012'

var client = new Twit(secret);

var stream = client.stream('statuses/filter', { follow: user_id})
console.log(stream)

stream.on('tweet', (tweet) => {
    if (tweet.user.id_str == user_id && tweet.in_reply_to_status_id == null) {
        client.post('statuses/update', {
            status: 'Ender WeirdChamp',
            in_reply_to_status_id: tweet.id_str,
            auto_populate_reply_metadata: true
        }, (err, data, res) => {
            if (err)
                console.log(err.message)
            else
                console.log('Success')
        })
    }
})

express()
    .get('/',  (req, res) => res.send("Ender WeirdChamp bot reporting for duty"))
    .listen(process.env.PORT, () => console.log(`Listening on ${process.env.PORT}`))

console.log('Bot listening..')